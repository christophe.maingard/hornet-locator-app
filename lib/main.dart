import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:hornet_locator_app/core/services/material-colors-generator.dart';
import 'package:hornet_locator_app/features/detection/screens/detection.screen.dart';
import 'package:hornet_locator_app/features/detection/screens/thanks.screen.dart';

import 'features/detection/screens/error-screen.screen.dart';

void main() {
  runApp(HornetDetector());
}

class HornetDetector extends StatelessWidget {
  final themeData = ThemeData(
    primarySwatch: generateMaterialColor(Color(0xFF001f3f)),
    /*visualDensity: VisualDensity.adaptivePlatformDensity,*/
  );
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    generateMaterialColor(Color(0x12FF15));
    return MaterialApp(
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      initialRoute: '/',
      routes: {
        '/': (context) => DetectionPage(),
        '/error': (context) => ErrorScreen(),
        '/thanks': (context) => ThanksScreen(),
      },
      supportedLocales: AppLocalizations.supportedLocales,
      title: 'Hornet detector',
      theme: themeData,
    );
  }
}

class DetectionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DetectionScreen();
  }
}
