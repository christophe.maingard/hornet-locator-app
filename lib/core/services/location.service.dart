import 'package:flutter/cupertino.dart';
import 'package:location/location.dart';

class LocationService {
  Location location = new Location();

  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;

  Future<bool> checkPermission() async {
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return false;
      }
    }
    return true;
  }

  Future<bool> checkService() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return false;
      }
    }
    return true;
  }

   Future<bool> checkServiceAndPermission() async {
    bool permissionGiven = await this.checkPermission();
    if (!permissionGiven) {
      return false;
    }

    bool serviceEnabled = await this.checkService();
    if (!serviceEnabled) {
      return false;
    }

    return permissionGiven && serviceEnabled;
  }

  Future<LocationData> getLocationData() async {
    bool locationDataAvailable = await this.checkServiceAndPermission();

    if (!locationDataAvailable) return null;

    _locationData = await location.getLocation();
    return _locationData;
  }
}
