enum DetectedType {
  HORNET,
  NEST
}

extension DetectedTypeExtension on DetectedType {

  String get name {
    switch (this) {
      case DetectedType.HORNET:
        return "HORNET";
      case DetectedType.NEST:
        return "NEST";
      default:
        return "null";
    }
  }
}
