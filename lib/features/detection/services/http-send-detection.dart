import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:hornet_locator_app/features/detection/models/detection.dart';

class HttpSendDetection {
  static Future<http.Response> sendDetection(Detection detection) async {
    var url = "https://europe-west1-hornet-locator.cloudfunctions.net/trigger-detected";
    var jsonDetection = jsonEncode(detection);
    return http.post(url, body: jsonDetection);
  }
}
