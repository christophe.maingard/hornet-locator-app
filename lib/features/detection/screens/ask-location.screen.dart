import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hornet_locator_app/core/widgets/blurry-dialog.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AskLocationWidget extends StatefulWidget {
  final Future<void> Function() askForLocation;

  const AskLocationWidget({this.askForLocation})
      : assert(askForLocation != null);

  @override
  _AskLocationWidget createState() => _AskLocationWidget(this.askForLocation);
}

class _AskLocationWidget extends State<AskLocationWidget> {
  BuildContext currentContext;

  _AskLocationWidget(continueCallBack);

  Future<void> showBlurryDialog() async {
    VoidCallback continueCallBack = () => {
          Navigator.of(context).pop(),
          // code on continue comes here
        };
    BlurryDialog alert = BlurryDialog("Please activate location",
        "Location is required to detect hornets and nests.", continueCallBack);
    showDialog(
        context: currentContext,
        builder: (BuildContext context) {
          return alert;
        });
  }

  @override
  Widget build(BuildContext context) {
    currentContext = context;
    return Scaffold(
        backgroundColor: Colors.white10,
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    AppLocalizations.of(context).askLocation,
                    style: TextStyle(fontSize: 30, color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                  ElevatedButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.white60)),
                      padding: const EdgeInsets.all(16.0),
                    ),
                    onPressed: () async => widget.askForLocation(),
                    child: Text(
                      AppLocalizations.of(context).allowLocationButton,
                      style: TextStyle(fontSize: 20),
                    ),
                  )
                ]),
          ),
        ));
  }
}
