import 'package:app_settings/app_settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hornet_locator_app/core/services/location.service.dart';
import 'package:hornet_locator_app/core/widgets/blurry-dialog.dart';
import 'package:hornet_locator_app/features/detection/models/detection-kind.dart';
import 'package:hornet_locator_app/features/detection/models/detection.dart';
import 'package:hornet_locator_app/features/detection/screens/ask-location.screen.dart';
import 'package:hornet_locator_app/features/detection/screens/error-screen.screen.dart';
import 'package:hornet_locator_app/features/detection/services/http-send-detection.dart';
import 'package:hornet_locator_app/features/detection/widget/detection-card.dart';
import 'package:latlong/latlong.dart';

class DetectionScreen extends StatefulWidget {
  @override
  _DetectionScreenState createState() => _DetectionScreenState();
}

class _DetectionScreenState extends State<DetectionScreen> {
  Future<bool> locationEnabledFuture;
  LocationService _locationService = new LocationService();
  var loading = false;
  var locationEnabled = false;
  var widgetContext;

  void sendDetected(String type, DetectionKind detectedKind) async {
    if (!locationEnabled) {
      await this.askForLocation();
    }
    var location = await _locationService.getLocationData();
    LatLng latLng = LatLng(location.latitude, location.longitude);
    var detection = Detection(type: type, location: latLng);
    try {
      var res = await HttpSendDetection.sendDetection(detection);
      debugPrint(res.body);
      Navigator.pushNamed(context, '/thanks', arguments: type);
    } catch (error) {
      Navigator.pushNamed(context, '/error',
          arguments:
              ErrorScreenArgs(detection: detection, retryFunction: HttpSendDetection.sendDetection));
    }
  }

  Future<void> initPlatformState() async {
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
  }

  List<DetectionKind> getDetectionKindList() {
    return detectedKindList;
  }

  Future<bool> askForLocation() async {
    locationEnabled = await _locationService.checkServiceAndPermission();
    if (!locationEnabled) {
      showBlurryDialog();
    }
    setState(() {
      this.locationEnabled = locationEnabled;
    });
    return locationEnabled;
  }

  Future<void> showBlurryDialog() async {
    VoidCallback continueCallBack = () {
      Navigator.of(context).pop();
      AppSettings.openLocationSettings();
    };
    BlurryDialog alert = BlurryDialog("Abort", "t'es sûr?", continueCallBack);
    showDialog(
        context: widgetContext,
        builder: (BuildContext context) {
          return alert;
        });
  }

  Widget buildDetectionWidget() {
    var detectedKindList = getDetectionKindList();

    return Scaffold(
        appBar: new AppBar(
          title: new Text("Hornet locator"),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
                child: Hero(
                    tag: detectedKindList[0].id,
                    child: DetectionCard(
                      detectedKind: detectedKindList[0],
                      press: () => this.sendDetected(
                          detectedKindList[0].id, detectedKindList[0]),
                    ))),
            Expanded(
              child: Hero(
                  tag: detectedKindList[1].id,
                  child: DetectionCard(
                    detectedKind: detectedKindList[1],
                    press: () => this.sendDetected(
                        detectedKindList[1].id, detectedKindList[1]),
                  )),
            )
          ],
        ));
  }

  Widget buildAskForLocationWidget() {
    return AskLocationWidget(askForLocation: askForLocation);
  }

  Widget getLocationBuilder(BuildContext context) {
    if (!locationEnabled) {
      return this.buildAskForLocationWidget();
    }
    return this.buildDetectionWidget();
  }

  @override
  void initState() {
    super.initState();

    initPlatformState();
    locationEnabledFuture = askForLocation();
  }

  @override
  Widget build(BuildContext context) {
    widgetContext = context;
    return this.getLocationBuilder(context);
  }
}
