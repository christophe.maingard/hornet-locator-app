import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hornet_locator_app/features/detection/models/detection.dart';
import 'package:hornet_locator_app/features/detection/widget/retry.widget.dart';
import 'package:hornet_locator_app/features/detection/widget/return.widget.dart';

class ErrorScreenArgs {
  final Detection detection;
  final Future<void> Function(Detection) retryFunction;

  ErrorScreenArgs({this.detection, this.retryFunction});
}

class ErrorScreen extends StatefulWidget {
  const ErrorScreen();

  @override
  _ErrorScreen createState() => _ErrorScreen();
}

class _ErrorScreen extends State<ErrorScreen> {
  Detection detection;
  Future<void> Function(Detection) retryFunction;

  bool alreadyRetried = false;
  BuildContext context;

  void retry() {
    try {
      retryFunction(detection);
      Navigator.pushReplacementNamed(context, '/thanks');
    } catch (error) {
      debugPrint(error);
    }
    setState(() {
      this.alreadyRetried = !this.alreadyRetried;
    });
  }

  Widget getRetryOrRestartWidget() {
    debugPrint(this.alreadyRetried.toString());
    if (!this.alreadyRetried) {
      return RetryWidget(onPress: () => this.retry());
    }
    return ReturnWidget(onPress: () => Navigator.pop(context));
  }

  void getArgs(BuildContext context) {
    final ErrorScreenArgs args = ModalRoute.of(context).settings.arguments;

    this.detection = args.detection;
    this.retryFunction = args.retryFunction;
  }

  @override
  Widget build(BuildContext context) {
    getArgs(context);
    this.context = context;
    return Scaffold(
        backgroundColor: Colors.red,
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child:  getRetryOrRestartWidget()),
          ),
        );
  }
}
