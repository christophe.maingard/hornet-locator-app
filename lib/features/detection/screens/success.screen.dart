import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SuccessScreen extends StatefulWidget {
  @override
  _SuccessScreen createState() => _SuccessScreen();
}

class _SuccessScreen extends State<SuccessScreen> {

  void navigateBack() {
    Navigator.pop(context);
  }

  List<Widget> returnSuccessWidget() {
    return <Widget>[
      Text(
        AppLocalizations.of(context).thankYou,
        style: TextStyle(fontSize: 60, color: Colors.white),
        textAlign: TextAlign.center,
      ),
      ElevatedButton(
        style: TextButton.styleFrom(
          primary: Colors.blue,
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
              side: BorderSide(color: Colors.white60)),
          padding: const EdgeInsets.all(16.0),
        ),
        onPressed: () => navigateBack(),
        child: Text(
          AppLocalizations.of(context).returnButton,
          style: TextStyle(fontSize: 40),
        ),
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blue,
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: returnSuccessWidget()),
          ),
        ));
  }
}
