import 'package:flutter/material.dart';
import 'package:hornet_locator_app/features/detection/widget/thanks.widget.dart';

class ThanksScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.blue,
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
            child: ThanksWidget(
          onPress: () => Navigator.pop(context),
        )),
      ),
    );
  }
}
