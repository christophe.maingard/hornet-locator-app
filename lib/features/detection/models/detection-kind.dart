import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hornet_locator_app/core/detected-type.dart';

class DetectionKind {
  final String id;
  final Color color;
  final String image;
  final String title;

  DetectionKind({this.id, this.color, this.image, this.title});
}

List<DetectionKind> detectedKindList = [
  DetectionKind(
    id: DetectedType.HORNET.name,
    color: Colors.amber,
    image: "assets/images/hornet.png",
    title: "Hornet "
  ),
  DetectionKind(
      id: DetectedType.NEST.name,
      color: Colors.redAccent,
      image: "assets/images/nest.png",
      title: "Nest"
  ),
];
