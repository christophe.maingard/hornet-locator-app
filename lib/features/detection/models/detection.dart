import 'package:latlong/latlong.dart';

class Detection {
  String type;
  LatLng location;

  Detection({this.type, this.location});

  Map<String, dynamic> toJson() => {
    "type": type,
    'location': {
      'latitude': location.latitude,
      'longitude': location.longitude
    }
  };
}
