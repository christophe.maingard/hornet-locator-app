import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ThanksWidget extends StatelessWidget {
  final Function onPress;

  const ThanksWidget({Key key, this.onPress}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          AppLocalizations.of(context).thankYou,
          style: TextStyle(fontSize: 60, color: Colors.white),
          textAlign: TextAlign.center,
        ),
        ElevatedButton(
          style: TextButton.styleFrom(
            primary: Colors.blue,
            backgroundColor: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
                side: BorderSide(color: Colors.white60)),
            padding: const EdgeInsets.all(16.0),
          ),
          onPressed: () => onPress(),
          child: Text(
            AppLocalizations.of(context).returnButton,
            style: TextStyle(fontSize: 40),
          ),
        )
      ],
    );
  }
}
