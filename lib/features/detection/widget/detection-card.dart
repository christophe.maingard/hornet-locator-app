import 'package:flutter/material.dart';
import 'package:hornet_locator_app/features/detection/models/detection-kind.dart';

class DetectionCard extends StatelessWidget {
  final DetectionKind detectedKind;
  final Function press;

  static const kTextColor = Color(0xFF535353);
  static const kTextLightColor = Color(0xFFACACAC);

  const DetectionCard({
    Key key,
    this.detectedKind,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.all(6.0),
              decoration: BoxDecoration(
                color: detectedKind.color,
              ),
                child: Image.asset(detectedKind.image),
            ),
          ),
        ],
      ),
    );
  }
}
